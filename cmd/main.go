package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/pocketbase/pocketbase"
	"github.com/pocketbase/pocketbase/plugins/migratecmd"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mai-archive-technologies/arch-tech-api/config"
	"gitlab.com/mai-archive-technologies/arch-tech-api/internal/common/logger"
	_ "gitlab.com/mai-archive-technologies/arch-tech-api/migrations"
)

func run() error {
	cfg := config.GetDefault()

	if err := logger.InitLogrus(cfg.Log.Level, cfg.Log.JsonFormat); err != nil {
		return fmt.Errorf("failed to init logger: %w", err)
	}
	log.Debug(cfg)

	app := pocketbase.New()

	// loosely check if it was executed using "go run"
	isGoRun := strings.HasPrefix(os.Args[0], os.TempDir())

	migratecmd.MustRegister(app, app.RootCmd, migratecmd.Config{
		// enable auto creation of migration files when making collection changes in the Admin UI
		// (the isGoRun check is to enable it only during development)
		Automigrate: isGoRun,
	})

	return app.Start()
}

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}
