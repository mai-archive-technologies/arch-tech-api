package migrations

import (
	"encoding/json"

	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase/daos"
	m "github.com/pocketbase/pocketbase/migrations"
	"github.com/pocketbase/pocketbase/models"
)

func init() {
	m.Register(func(db dbx.Builder) error {
		jsonData := `{
			"id": "rgkfi2vsl28ua7y",
			"created": "2024-01-18 14:34:34.552Z",
			"updated": "2024-01-18 14:34:34.552Z",
			"name": "archive",
			"type": "base",
			"system": false,
			"schema": [
				{
					"system": false,
					"id": "wyyr4lz4",
					"name": "doc_name",
					"type": "text",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"min": null,
						"max": null,
						"pattern": ""
					}
				},
				{
					"system": false,
					"id": "mliuj79d",
					"name": "owner",
					"type": "relation",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"collectionId": "_pb_users_auth_",
						"cascadeDelete": false,
						"minSelect": null,
						"maxSelect": 1,
						"displayFields": null
					}
				},
				{
					"system": false,
					"id": "vkegctgl",
					"name": "reg_num",
					"type": "number",
					"required": false,
					"presentable": false,
					"unique": false,
					"options": {
						"min": null,
						"max": null,
						"noDecimal": false
					}
				},
				{
					"system": false,
					"id": "x4dpf8ke",
					"name": "reg_date",
					"type": "date",
					"required": false,
					"presentable": false,
					"unique": false,
					"options": {
						"min": "",
						"max": ""
					}
				},
				{
					"system": false,
					"id": "lkuwuqaj",
					"name": "description",
					"type": "text",
					"required": false,
					"presentable": false,
					"unique": false,
					"options": {
						"min": null,
						"max": null,
						"pattern": ""
					}
				},
				{
					"system": false,
					"id": "ikl05ezz",
					"name": "file",
					"type": "file",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"mimeTypes": [],
						"thumbs": [],
						"maxSelect": 1,
						"maxSize": 5242880,
						"protected": false
					}
				}
			],
			"indexes": [
				"CREATE UNIQUE INDEX ` + "`" + `idx_nEZOdEu` + "`" + ` ON ` + "`" + `archive` + "`" + ` (` + "`" + `reg_num` + "`" + `)"
			],
			"listRule": "",
			"viewRule": "",
			"createRule": "@request.auth.verified = true",
			"updateRule": "@request.auth.verified = true && owner.id = @request.auth.id",
			"deleteRule": "@request.auth.verified = true && owner.id = @request.auth.id",
			"options": {}
		}`

		collection := &models.Collection{}
		if err := json.Unmarshal([]byte(jsonData), &collection); err != nil {
			return err
		}

		return daos.New(db).SaveCollection(collection)
	}, func(db dbx.Builder) error {
		dao := daos.New(db)

		collection, err := dao.FindCollectionByNameOrId("rgkfi2vsl28ua7y")
		if err != nil {
			return err
		}

		return dao.DeleteCollection(collection)
	})
}
