package logger

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
)

var (
	ErrLogLevelUnknown = errors.New("log level unknown")
)

func InitLogrus(logLevel string, logJson bool) error {
	if logJson {
		logrus.SetFormatter(&logrus.JSONFormatter{
			TimestampFormat: "2006-01-02 15:04:05",
		})
	} else {
		logrus.SetFormatter(&logrus.TextFormatter{
			TimestampFormat: "2006-01-02 15:04:05",
			FullTimestamp:   true,
		})
	}

	logrus.SetOutput(os.Stdout)

	var level logrus.Level
	switch strings.ToLower(logLevel) {
	case "debug":
		level = logrus.DebugLevel

	case "info":
		level = logrus.InfoLevel

	case "warn":
		level = logrus.WarnLevel

	default:
		return fmt.Errorf("%w: %s", ErrLogLevelUnknown, logLevel)
	}
	logrus.SetLevel(level)
	return nil
}
