# [1. GOLANG BUILD STAGE]
FROM golang:1.21-alpine AS builder
ARG CI_COMMIT_SHORT_SHA

WORKDIR /workspace

COPY . /workspace/

ENV CGO_ENABLED=0
RUN go build  \
      -ldflags=" \
        -X 'gitlab.wildberries.ru/wb-tile-server/wb-tile-server/geocoding/geocoding-api/config.hashVersion=${CI_COMMIT_SHORT_SHA}'" \
      -o /workspace/archive \
    cmd/main.go

# [2. DEPLOY STAGE]
FROM alpine

#go app
COPY --from=builder /workspace/archive /workspace/archive
COPY --from=builder etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

RUN ["chmod", "+x", "/workspace/archive"]

EXPOSE 8080

CMD ["/workspace/archive", "serve", "--http", "0.0.0.0:8080"]
