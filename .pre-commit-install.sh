#!/bin/bash
# binary will be $(go env GOPATH)/bin/golangci-lint
golangci-lint --version || (curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh \
| sh -s -- -b "$(go env GOPATH)/bin" v1.55.2)

command -v goimports || go install golang.org/x/tools/cmd/goimports@latest

command -v gocyclo || go install github.com/fzipp/gocyclo/cmd/gocyclo@latest
