package config

import (
	"encoding/json"
	"log"
	"sync"
	"time"

	"github.com/kelseyhightower/envconfig"
)

type (
	Config struct {
		App
		Log
		DB
	}

	App struct {
		Version string `envconfig:"APP_VERSION" default:"1.0.0"`
		// deploy variables
		HashCommit string
		// probes
		StartTime time.Time
	}

	Log struct {
		Level      string `envconfig:"LOG_LEVEL" default:"info"`
		JsonFormat bool   `envconfig:"LOG_JSON_FORMAT" default:"false"`
	}

	DB struct {
		Version string
	}
)

var (
	cfg        Config
	once       sync.Once
	hashCommit string
)

func GetDefault() *Config {
	once.Do(func() {
		if err := envconfig.Process("", &cfg); err != nil {
			log.Fatal(err)
		}
	})

	cfg.App.HashCommit = hashCommit

	cfg.App.StartTime = time.Now()

	return &cfg
}

func (c Config) String() string {
	str, _ := json.Marshal(c)
	return string(str)
}
